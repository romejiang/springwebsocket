package com.romejiang.models;

/**
 * websocket 发过来的挖矿信息，多种挖矿信息
 */
public class MiningInfo {
    String height;
    String generationSignature;
    String baseTarget;
    String blockchain;
    String scoop;

    public MiningInfo(){

    }

    public MiningInfo(String height, String generationSignature, String baseTarget, String blockchain, String scoop) {
        this.height = height;
        this.generationSignature = generationSignature;
        this.baseTarget = baseTarget;
        this.blockchain = blockchain;
        this.scoop = scoop;
    }

    @Override
    public String toString() {
        return "MiningInfo{" +
                "height='" + height + '\'' +
                ", generationSignature='" + generationSignature + '\'' +
                ", baseTarget='" + baseTarget + '\'' +
                ", blockchain='" + blockchain + '\'' +
                ", scoop='" + scoop + '\'' +
                '}';
    }

    public String getScoop() {
        return scoop;
    }

    public void setScoop(String scoop) {
        this.scoop = scoop;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getGenerationSignature() {
        return generationSignature;
    }

    public void setGenerationSignature(String generationSignature) {
        this.generationSignature = generationSignature;
    }

    public String getBaseTarget() {
        return baseTarget;
    }

    public void setBaseTarget(String baseTarget) {
        this.baseTarget = baseTarget;
    }

    public String getBlockchain() {
        return blockchain;
    }

    public void setBlockchain(String blockchain) {
        this.blockchain = blockchain;
    }
}
