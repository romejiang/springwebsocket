package com.romejiang.models;

/**
 * 基于websocket 发送给 挖矿的结果内容。带着区块链的名称，硬盘大小等信息
 */
public class SubmitNonce {
    String blockchain;
    String maxscoop;
    String nonce;
    String blockheight;
    String accountId;

    public SubmitNonce() {
    }

    public SubmitNonce(String blockchain, String maxscoop, String nonce, String blockheight, String accountId) {
        this.blockchain = blockchain;
        this.maxscoop = maxscoop;
        this.nonce = nonce;
        this.blockheight = blockheight;
        this.accountId = accountId;
    }

    public String getBlockchain() {
        return blockchain;
    }

    public void setBlockchain(String blockchain) {
        this.blockchain = blockchain;
    }

    public String getMaxscoop() {
        return maxscoop;
    }

    public void setMaxscoop(String maxscoop) {
        this.maxscoop = maxscoop;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getBlockheight() {
        return blockheight;
    }

    public void setBlockheight(String blockheight) {
        this.blockheight = blockheight;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
