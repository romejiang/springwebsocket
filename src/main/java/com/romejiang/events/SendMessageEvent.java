package com.romejiang.events;


import org.yeauty.pojo.Session;

public class SendMessageEvent {
    private String message;
    private Session session;

    public SendMessageEvent(Session session, String message)
    {
        this.message = message;
        this.session = session;
    }

    public String getBlocks()
    {
        return message;
    }

}
