package com.romejiang.events;

import com.romejiang.models.MiningInfo;
import org.yeauty.pojo.Session;

/**
 * Created by stephen on 2019/8/4.
 */
public class ReceiveMessageEvent {
    private MiningInfo message;

    private Session session;

    public ReceiveMessageEvent(Session session, MiningInfo message) {
        this.message = message;
        this.session = session;
    }

    public MiningInfo getMessage() {
        return message;
    }

    public Session getSession() {
        return session;
    }
}
